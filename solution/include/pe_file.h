/// @brief File contains structure of PE file
/// @file

#ifndef SECTION_EXTRACTOR_PE_FILE_C
#define SECTION_EXTRACTOR_PE_FILE_C

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// PE file signature offset
#define SIGNATURE_OFFSET 0x3c

/// PE file signature
#define PE_FILE_SIGNATURE 0x00004550

/// Maximum length of section name
#define MAX_SECTION_NAME_LEN 8

#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
/**
 * @brief A packed structure that holds data from the PE header
 * The PE header provides information about an executable file,
 * including the machine type, number of sections, creation time stamp,
 * symbol table information, and file attributes
 * @note The struct is packed, so its members will be correctly aligned
 */
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
PEHeader {

    /// The number that identifies the type of target machine
    int16_t machine;

    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    int16_t number_of_sections;

    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    int32_t time_date_stamp;

    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    int32_t pointer_to_symbol_table;

    /// The number of entries in the symbol table
    int32_t number_of_symbols;

    /// The size of the optional header, which is required for executable files but not for object files
    int16_t size_of_optional_header;

    /// The flags that indicate the attributes of the file
    int16_t characteristics;

};

/**
 * @brief A packed structure that represents a section header in a PE file
 * The section header contains information about a specific section in the PE file
 * @note The struct is packed, so its members will be correctly aligned
 */
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
SectionHeader {

    /// The name of the section
    uint8_t name[MAX_SECTION_NAME_LEN];

    /// The total size of the section when loaded into memory
    union {
        /// The physical address of the section
        uint32_t physical_address;

        /// The virtual size of the section
        uint32_t virtual_size;
    } misc;

    /// The virtual address of the section
    uint32_t virtual_address;

    /// The size of the section in the raw data file
    uint32_t size_of_raw_data;

    /// The file pointer to the section's raw data
    uint32_t pointer_to_raw_data;

    /// The file pointer to the section's relocation entries
    uint32_t pointer_to_relocations;

    /// The file pointer to the section's line-number entries
    uint32_t pointer_to_linenumbers;

    /// The number of relocation entries for the section
    uint16_t number_of_relocations;

    /// The number of line-number entries for the section
    uint16_t number_of_linenumbers;

    /// The characteristics of the section, such as permissions and flags
    uint32_t characteristics;

};

#ifdef _MSC_VER
    #pragma pack(pop)
#endif

/**
 * @brief Structure containing PE file data
 * Used to represent the entire PE file, with a
 * signature identifying it as a PE format image file,
 * main header and section headers
 */
struct PEFile {

    /// Magic number that identifies the file as a PE format image file
    uint32_t signature;

    /// Main header
    struct PEHeader header;

    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;

};

/**
 * @brief Parses and extracts relevant information from a PE file,
 * storing it in a PE file data structure
 * @param[in] in Pointer to the input PE file
 * @param[in,out] file Pointer to the PE file data structure where the read data will be stored
 * @return 0 if the function executes successfully, or 1 if an error occurs
 */
int16_t read_pe_file(FILE *in, struct PEFile *file);

/**
 * @brief Searches for a specific section within a PE file by its name
 * @param[in] file Pointer to the PE file data structure
 * @param[in] name Name of the section to search for
 * @return section index or -1 in case if section wasn't found
 */
int16_t find_section_by_name(const struct PEFile *file, const char *name);

/**
 * @brief Extracts the binary data of a specific section from an input PE file
 * and writes it to an output file
 * @param[in] in Pointer to the input PE file
 * @param[in] out Pointer to the output binary file
 * @param[in] section_header Section header data structure for the extracted section
 */
void write_section_data(FILE *in, FILE *out, struct SectionHeader section_header);

#endif
