/// @brief File contains structure of PE file
/// @file

#include "pe_file.h"

/**
 * @brief Parses and extracts relevant information from a PE file,
 * storing it in a PE file data structure
 * @param[in] in Pointer to the input PE file
 * @param[in,out] file Pointer to the PE file data structure where the read data will be stored
 * @return 0 if the function executes successfully, or 1 if an error occurs
 */
int16_t read_pe_file(FILE *in, struct PEFile *file) {
    if (!in) return 1;

    fseek(in, SIGNATURE_OFFSET, SEEK_SET);

    uint32_t offset;
    fread(&offset, sizeof(uint32_t), 1, in);
    fseek(in, offset, SEEK_SET);

    fread(&file->signature, sizeof(file->signature), 1, in);
    if (file->signature != PE_FILE_SIGNATURE) return 1;

    fread(&file->header, sizeof(file->header), 1, in);
    file->section_headers = malloc(file->header.number_of_sections * sizeof(struct SectionHeader));
    if (!file->section_headers) return 1;
    fseek(in, file->header.size_of_optional_header, SEEK_CUR);
    fread(file->section_headers, sizeof(struct SectionHeader), file->header.number_of_sections, in);
    return 0;
}

/**
 * @brief Searches for a specific section within a PE file by its name
 * @param[in] file Pointer to the PE file data structure
 * @param[in] name Name of the section to search for
 * @return section index or -1 in case if section wasn't found
 */
int16_t find_section_by_name(const struct PEFile *file, const char *name) {
    for (int16_t i = 0; i < file->header.number_of_sections; i++) {
        if (memcmp(file->section_headers[i].name, name, strlen(name)) != 0) continue;
        return i;
    }
    return -1;
}

/**
 * @brief Extracts the binary data of a specific section from an input PE file
 * and writes it to an output file
 * @param[in] in Pointer to the input PE file
 * @param[in] out Pointer to the output binary file
 * @param[in] section_header Section header data structure for the extracted section
 */
void write_section_data(FILE *in, FILE *out, const struct SectionHeader section_header) {
    if (section_header.pointer_to_raw_data == 0) return;

    fseek(in, section_header.pointer_to_raw_data, SEEK_SET);

    char *section = malloc(section_header.size_of_raw_data);
    if (!section) return;

    fread(section, section_header.size_of_raw_data, 1, in);
    fwrite(section, section_header.size_of_raw_data, 1, out);
    free(section);
}
