/// @brief Main application file
/// @file

#include "pe_file.h"

/// String with application name
#define APP_NAME "section-extractor"

/**
 * @brief Main function for the application
 * @param[in] argc Number of arguments passed through the command line
 * @param[in] argv Array of strings containing the command line arguments
 * @return 0 if the program executes successfully, an error code if not
 */
int main(int argc, char **argv) {
   if (argc != 4) {
       fprintf(stderr, "Usage: " APP_NAME " <source-pe-file> <section-name> <output-bin-image>\n");
       return 1;
   }
   struct PEFile *file = malloc(sizeof(struct PEFile));
   if (!file) return 1;
   FILE *in = fopen(argv[1], "rb");
   if (!in) {
       printf("Error occurred while opening input file.");
       free(file);
       return 1;
   }
   int16_t code = read_pe_file(in, file);
   if (code != 0) {
       printf("Error occurred while reading PE file.");
       free(file);
       fclose(in);
       return 1;
   }
   char *section_name = argv[2];
   int16_t section_id = find_section_by_name(file, section_name);
   if (section_id == -1) {
       printf("Section with given name wasn't found.");
       free(file->section_headers);
       free(file);
       fclose(in);
       return 1;
   }
   FILE *out = fopen(argv[3], "wb");
   if (!out) {
       printf("Error occurred while opening output file.");
       free(file->section_headers);
       free(file);
       fclose(in);
       return 1;
   }
   write_section_data(in, out, file->section_headers[section_id]);
   printf("All data has been written successfully.");
   free(file->section_headers);
   free(file);
   fclose(in);
   fclose(out);
   return 0;
}
